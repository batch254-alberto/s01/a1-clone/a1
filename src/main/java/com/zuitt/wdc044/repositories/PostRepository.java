package com.zuitt.wdc044.repositories;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


// an interface contains behavior that a class implements
// an interface marked as @Repository contains methods for "database manipulation"
// by extending CrudRepository, the PostRepository interface has inherited its pre-defined methods for creating, retrieving, updating, and deleting records.
@Repository
public interface PostRepository extends CrudRepository<Post, Object> {

    Iterable<Post> findByUser(User user);
}
