package com.zuitt.wdc044.controllers;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.services.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class PostController {

    @Autowired
    PostService postService;

    @RequestMapping(value="/posts", method= RequestMethod.POST)
    //"ResponseEntity" represent the whole HTTP response: status code, headers, and body.
    public ResponseEntity<Object> createPost(@RequestHeader(value="Authorization") String stringToken, @RequestBody Post post) {

        // We can access the "postService" methods and pass the following arguments:
            //stringToken of the current session will ve retrieved from the request headers.
            //a "post" object will be instantiated upon receiving the request body, and this will follow the properties defined in the Post model.
        // note: the "key" name of the request from postman should be similar to the property names defined in the model.
        postService.createPost(stringToken, post);
        return new ResponseEntity<>("Post created successfully", HttpStatus.CREATED);

    }

    // Get all posts
    @RequestMapping(value="/posts", method = RequestMethod.GET)
    public ResponseEntity<Iterable<Post>> getPosts() {
        Iterable<Post> posts = postService.getPosts();

        for(Post post : posts) {
            post.getUser();
        }
        return ResponseEntity.ok().body(posts);
    }

    // Edit a post
    @RequestMapping(value="/posts/{postId}", method = RequestMethod.PUT)
        // @PathVariable is used for data passed in the URI
            //@RequestParam vs @PathVariable
            //@RequestParam is used to extract data found in the query parameters commonly used for filtering the results based on the condition
            //@PathVariable is used to extract exact record.
    // returns an object type<Object>
    public ResponseEntity<Object> updatePost(@PathVariable Long postId,  @RequestHeader(value="Authorization") String stringToken,@RequestBody Post post ) {

        return postService.updatePost(postId, stringToken, post);
    }

    // Delete a post
    @RequestMapping(value="/posts/{postId}", method = RequestMethod.DELETE)
    // return an object type
    public ResponseEntity<Object> deletePost(@PathVariable Long postId, @RequestHeader(value="Authorization") String stringToken) {

        return postService.deletePost(postId, stringToken);

    }

    // Retrieve a user post
    @RequestMapping(value="/myPosts", method = RequestMethod.GET)
    public ResponseEntity<Object> getPostsByUser(@RequestHeader(value="Authorization") String stringToken) {
        return new ResponseEntity<>(postService.getPostsByUser(stringToken), HttpStatus.OK);
    }

}
