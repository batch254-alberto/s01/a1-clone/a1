package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    //create a post
    void createPost(String stringToken, Post post);

    // Get a user post
    Iterable<Post> getPostsByUser(String stringToken);

    //getting all posts
    Iterable<Post> getPosts();

    // Edit a user post button no logic yet
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a user post
    ResponseEntity deletePost(Long id, String stringToken);




}
